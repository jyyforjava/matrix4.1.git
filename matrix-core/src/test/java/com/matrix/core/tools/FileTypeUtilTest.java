package com.matrix.core.tools;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class FileTypeUtilTest {

	@Test
	public void testFileType() throws IOException {
		
		List<FileType> fileTypes = new ArrayList<>();
		fileTypes.add(FileType.PNG);
		assertTrue(FileTypeUtil.checkFileType(new File("/Users/jiangyouyao/Downloads/2.png"), fileTypes));

	}

}
