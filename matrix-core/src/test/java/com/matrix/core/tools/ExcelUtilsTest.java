package com.matrix.core.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.matrix.core.tools.excl.ExcelSheetPO;
import com.matrix.core.tools.excl.ExcelUtil;
import com.matrix.core.tools.excl.ExcelVersion;

public class ExcelUtilsTest {

	@Test
	public void testCreateWorkbook() throws IOException {

		long begin = System.currentTimeMillis();

		List<List<Object>> dataList = new ArrayList<>();
		String heads[] = new String[ExcelVersion.V2007.getMaxColumn()];
		for (int i = 0; i < ExcelVersion.V2007.getMaxColumn(); i++) {
			heads[i] = i + "";
		}

		for (int i = 0; i < ExcelVersion.V2007.getMaxRow(); i++) {
			List<Object> row = new ArrayList<>();
			for (int j = 0; j < ExcelVersion.V2007.getMaxColumn(); j++) {
				row.add(j);
			}
			dataList.add(row);
		}

		ExcelSheetPO ex = new ExcelSheetPO();
		ex.setTitle("test");
		ex.setDataList(dataList);
		ex.setSheetName("ces");
		ex.setHeaders(heads);

		ExcelUtil.createWorkbookAtDisk(ExcelVersion.V2007, Arrays.asList(ex),
				"/Users/jiangyouyao/1" + ExcelVersion.V2007.getSuffix());
		System.out.println(System.currentTimeMillis() - begin);
	}

	@Test
	public void testReadExcel() {
		File f = new File("/Users/jiangyouyao/123.xlsx");
		try {
			List<ExcelSheetPO> s = ExcelUtil.readExcel(f, null, null);
			Assert.assertNotNull(s.get(0).getSheetName());
		} catch (IOException e) {
		}
	}
}
