package com.matrix.core.tools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.Key;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
/**
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年5月13日
 */
public class RSAUtilsTest {

	private String publicKey;
    private String privateKey;

    @Before
    public void setUp() throws Exception {
        Map<String, Key> keyMap = RSAUtils.initKey();
        publicKey = RSAUtils.getPublicKey(keyMap);
        privateKey = RSAUtils.getPrivateKey(keyMap);
        System.err.println("公钥: \n\r" + publicKey);
        System.err.println("私钥： \n\r" + privateKey);
    }

    @Test
    public void createKey(){
    	
    }
    
    @Test
    public void testDecrypt() throws Exception{
    	String pwd="LjQwkyVKUwcGpyG3k3R+eqru2iO75T3usvzUv+IwO4Hzp6VpTUcSu4PK69ZMFs6ZoasoVV8sCP5shtvk7xaFCTPxY5oTcf+FcKeKgj8aEBXcGvP4y2VpZPWDZPPPTogaOzGGZ/RxMx10GSC7Fhnauvt6faoKzhPtEFmwIvGneio=";
    	  byte[] decodedData = RSAUtils.decryptByPrivateKey(pwd,
                  "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALouRL+UyXZ4pbMIm0PQyxKS8xlKA5xr++swGii7vfnyhj5WIq4W/ny8tdHqJLx2dOhzFbwCgoJ22U70awBtQuMcn9mgaqWJ6h60IdH/xSHZ/GSTCudB1seoe3clY0Zqwc37jFZzkuzdUNfdhdp9E3H3fJMRXWzT4dlAuwG2rxgDAgMBAAECgYAamU+jG0upDFWH/XvJNYQ9tBbdDxDzAQ5trGKgXSooEnkRYNb1jj1vbrp/MfdroXTUXNTHFoi0wOwM9qhu2C1M7LwmFHJE2OCtN3BZrlnMPmq+bKpto3RtYPCOOY+8TOR+wxdPV2tgMisRHN+rg2QcwmzyZQxHvBShXV1Q9WNNWQJBAOAD+7We8LtOeulIrY3E6wAXawfDQ4B1QW9/DJSLKmZFSOo0DzxYSa5PJ3ZY68q+mxkogatzgOCRdc5SSHN2050CQQDUw2FxD6Rs5uGp1NGwtaMlRpa/V6dQIUakqMYMPPoDwPcmA17YqXa2ETYh+s33ixvBN7E11CLltxlS5JKVC9gfAkB5QI6w6tvgdoU+2hDeqJSOkx5isEVMNat6fyk/CeikPKJP6mfIwPz4tW1luJHARtVKk8tEExvR8cnzKESBAvNdAkBcJpdwoabEqCc7KHr9v13/ChjGnQ9RbGgtbxAAQvTCZXRmGXWd/5/z8XZxtzA4NB78S8PMHapTtj8YoPk/5tZfAkEAlZ1hEpuX6L1i5wx5F1nzHMCAD9SeSxdq7CwP28mhhsi6prVd+yqmOTKLD0JYYeDjpgSV4R9gAG2AQrZnI+9Y3A==");
          String outputStr = new String(decodedData);
          System.err.println("加密前: " + pwd + "\n\r" + "解密后: " + outputStr);
    }
    
    @Test
    public void test() throws Exception {
        System.err.println("公钥加密——私钥解密");
        String inputStr = "abc";
        byte[] encodedData = RSAUtils.encryptByPublicKey(inputStr, publicKey);
        byte[] decodedData = RSAUtils.decryptByPrivateKey(encodedData,
                privateKey);
        String outputStr = new String(decodedData);
        System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);
        assertEquals(inputStr, outputStr);
    }

    @Test
    public void testSign() throws Exception {
        System.err.println("私钥加密——公钥解密");
        String inputStr = "sign";
        byte[] data = inputStr.getBytes();
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
        String outputStr = new String(decodedData);
        System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);
        assertEquals(inputStr, outputStr);
        System.err.println("私钥签名——公钥验证签名");
        // 产生签名
        String sign = RSAUtils.sign(encodedData, privateKey);
        System.err.println("签名:" + sign);
        // 验证签名
        boolean status = RSAUtils.verify(encodedData, publicKey, sign);
        System.err.println("状态:" + status);
        assertTrue(status);
    }

}
