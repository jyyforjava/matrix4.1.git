package com.matrix.core.constance;

/**
 * 系统信息码
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public interface SystemMessageCode {

	/** =======系统信息码=========== **/

	/** 添加成功 */
	String ADD_SUCCES = "1000_0000";
	/** 修改成功 */
	String UPDATE_SUCCES = "1000_0001";
	/** 成功删除{0}条数据 */
	String DELETE_SUCCES = "1000_0002";

	 
}
