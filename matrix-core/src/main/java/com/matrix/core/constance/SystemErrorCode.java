package com.matrix.core.constance;

/**
 * 系统错误码
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public interface SystemErrorCode {

	/** =======系统错误码=========== **/
	/** 自定义异常信息 **/
	String SYSTEM_ERROR_MSG = "700000";
	/** 系统未知异常 */
	String SYSTEM_UNKNOW_ERROR = "700001";

	/** 系统运行异常，参数{0}不能为空 */
	String SYSTEM_NULLF_ERROR = "700002";

	/** 系统运行异常 **/
	String SYSTEM_RUNNING_ERROR = "700003";

	/** {0}操作失败 **/
	String SYSTEM_OPERATION_FAIL = "700004";

	/** {0}无效数据 **/
	String INVALID_DATA = "700005";

	/** 权限不足 **/
	String PERMISSION_DENIED = "700006";

	/** {0}格式错误 **/
	String FORMAT_ERROR = "700007";

	/** {0}数据修改失败 **/
	String DATA_UPDATE_FAIL = "700008";

	/** {0}数据新增失败 **/
	String DATA_ADD_FAIL = "700009";

	/** {0}数据删除失败 **/
	String DATA_DELETE_FAIL = "700010";
	
	/** {0}数据重复 **/
	String DATA_REPEAT = "700011";
	/** 请求失效 **/
	String REQUEST_INVALID = "700012";

}
