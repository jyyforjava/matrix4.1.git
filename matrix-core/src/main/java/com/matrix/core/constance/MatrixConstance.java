package com.matrix.core.constance;

/**
 * 
 * @Description:matrix常量类
 * @author:姜友瑶
 * @date 2016年8月28日
 */
public class MatrixConstance {

	public final static String LOGIN_KEY = "userInfo";
	/**
	 * 是否为debug模式
	 */
	public  static boolean DEBUG = false;

	/**
	 * 系统用户，用来标识一些操作是系统自己完成的
	 */
	public final static String SYSTEM_USER = "system";

	/** 创建人 **/
	public final static String CREATEBY = "createBy";

	/** 修改人 **/
	public final static String UPDATEBY = "updateBy";

	/**
	 * 数据操作成功
	 */
	public final static int DML_SUCCESSS = 1;

	/**
	 * 默认集合的大小
	 */
	public final static int COLLECTION_SIZE = 30;

	/**
	 * 加密私钥
	 */
	public final static String PRIVATE_KEY = "privateKey";
	/**
	 * 加密公钥
	 */
	public final static String PUPBLIC_KEY = "publicKey";

	public synchronized static void setDebugflag(boolean flag) {
		DEBUG = flag;
	}

}
