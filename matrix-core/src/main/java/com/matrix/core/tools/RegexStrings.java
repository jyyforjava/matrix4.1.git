package com.matrix.core.tools;
/**
 * 常用正则表达式
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public class RegexStrings {
	
	/**全数字**/
	public final static String ONLE_BUMBER="^[0-9]*$";
	
	/** 身份证15位**/
	public final static String IDCARD_15="^[1-9]\\d{13}[Xx|0-9]$";
	
	/** 身份证18位**/
	public final  static String IDCARD_18="^[1-9]\\d{16}[Xx|0-9]$";
	
	/** 固话号码，支持400 或 800开头**/
	public final  static String HOME_TELEPHONE="/^0?(13[0-9]|15[012356789]|17[013678]|18[0-9]|14[57])[0-9]{8}$/";
	
	/** 手机号码，支持17开头 **/
	public final  static String MOBILE_PHONE="/^0?(13[0-9]|15[012356789]|17[013678]|18[0-9]|14[57])[0-9]{8}$/";
}
