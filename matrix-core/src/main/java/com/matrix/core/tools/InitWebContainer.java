package com.matrix.core.tools;

import java.util.Locale;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.ServletContextAware;

import com.matrix.core.constance.MatrixConstance;

/**
 * 
 * @Description: 初始化web容器的类 一些网站启动需要加载的数据和方法在这里配置和执行
 * @author:姜友瑶
 * @date 2016年10月18日
 */
@Controller
public class InitWebContainer   implements ServletContextAware{

	private static final String TRUE = "true";

	private ServletContext servletContext;

	static private final String LANGUAGE_ZH = "zh";

	static private final String LANGUAGE_US = "us";
	/**
	 * 是否为debug模式
	 */
	@Value("${debug}")
	private String debug;

	@Value("${system_language}")
	private String systemLanguage;

	/**
	 * 
	 * @Description: 容器启动后加载数据
	 * @author:姜友瑶
	 * @param sc
	 * @date 2016年10月18日
	 */
	@Override
	public void setServletContext(ServletContext sc) {
		this.servletContext = sc;

		// 初始化调试模式
		initDebug();
		// 初始化语言环境
		initLanguage();

		LogUtil.info("\r\n\r\n**********************************************\r\n" 
				+ "* =========== Matrix启动成功 ===========\r\n" 
				+ "* DEBUG模式:" + debug+"*\r\n"
				+ "* 语言环境:" + Locale.getDefault().getLanguage()+"\r\n"
			 
				+ "**********************************************\r\n");
	}

	private void initDebug() {
		if (TRUE.equals(debug)) {
			MatrixConstance.DEBUG = true;
		} else {
			MatrixConstance.DEBUG = false;
		}
	}

	/**
	 * 初始化语言环境，默认为中文
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 */
	private void initLanguage() {
		Locale language = Locale.CHINA;
		if (systemLanguage != null) {
			if (LANGUAGE_US.equals(systemLanguage)) {
				language = Locale.US;
			}
		} else {
			LogUtil.warn("没有找到语言环境配置信息，默认配置中文环境");
		}
		Locale.setDefault(language);
	}
}
