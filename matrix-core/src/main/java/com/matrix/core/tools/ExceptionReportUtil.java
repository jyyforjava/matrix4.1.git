package com.matrix.core.tools;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

/**
 * 钉钉机器人工具
 * 
 * @author 李广林
 * @email 935090232@qq.com
 * @date 2018年5月9日
 */
public class ExceptionReportUtil {

	public static void sendMsg(String exceptionCenterUrl , String content) {
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost(exceptionCenterUrl);
		httppost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			StringEntity se = new StringEntity(content, "utf-8");
			se.setContentType("application/x-www-form-urlencoded");
			httppost.setEntity(se);
			HttpResponse response;
			response = httpclient.execute(httppost);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity(), "utf-8");
				LogUtil.debug("消息发送结果=" + result);

			} else {
				LogUtil.info("消息发送失败网络错误 状态码={}", response.getStatusLine().getStatusCode());
			}
		} catch (IOException e) {
			LogUtil.error("消息发送失败", e, "");
		}
	}

}
