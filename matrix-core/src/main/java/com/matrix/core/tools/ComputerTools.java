package com.matrix.core.tools;

import java.net.InetAddress;
import java.net.UnknownHostException;
/**
 * 计算机工具类
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年5月11日
 */
public class ComputerTools {
	public static String getHostName() {
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		}
		return hostName;
	}
}
