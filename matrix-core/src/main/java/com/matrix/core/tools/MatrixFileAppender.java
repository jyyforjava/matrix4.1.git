package com.matrix.core.tools;

import org.apache.log4j.DailyRollingFileAppender;

/**
 * 日志记录适配器
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月27日
 */
public class MatrixFileAppender extends DailyRollingFileAppender {

	/**
	 * 设置相对服务器的日志路径
	 */
	@Override
	public void setFile(String file) {
		String val = file.trim();
		fileName = PropertiesUtil.getString("log_path") + val;
	}
}
