package com.matrix.core.tools;

import org.apache.log4j.MDC;

/**
 * 日志追踪
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月27日
 */
public class MdcUtil {
	private static final String TR_KEY = "Tr";

	public static void setRequestId() {
		MDC.clear();
		MDC.put(TR_KEY, "TR = " + StringUtils.getRandomString(16) + "");
	}

	public static void clearRequestId() {
		MDC.clear();
	}

	public static String getMdc() {
		return (String) MDC.get(TR_KEY);
	}

}
