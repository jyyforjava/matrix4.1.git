package com.matrix.core.tools;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

/**
 * 读取定义的属性文件
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public class PropertiesUtil {

	private static final String SYSTEM_PROPERTIES_FILE = "config/core/system.properties";

	/**
	 * 获取system.properties中定义信息
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月28日
	 * @param key
	 * @return
	 */
	public static String getString(String key) {
		Properties prop = null;
		try {
			Resource resource = new ClassPathResource(SYSTEM_PROPERTIES_FILE);
			EncodedResource encodedResource = new EncodedResource(resource, "UTF-8");
			prop = PropertiesLoaderUtils.loadProperties(encodedResource);
		} catch (IOException e) {
			LogUtil.error(e.getMessage(), e);
		}
		if (prop != null) {
			return prop.getProperty(key);
		}
		return null;
	}

	
}
