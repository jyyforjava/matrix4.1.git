package com.matrix.core.tools;

/**
 * 文件头对应类型枚举
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年12月12日
 */
public enum FileType {

	
	JPEG("FFD8FF"),

	PNG("89504E47"),

	GIF("47494638"),

	TIFF("49492A00"),

	BPM("424D"),

	DWG("41433130"),

	PSD("38435053"),

	XML("3C3F786D6C"),

	HTML("68746D6C3E"),

	PDF("255044462D312E"),

	ZIP("504B0304"),

	RAR("52617221"),

	WAV("57415645"),

	AVI("41564920");

	private String value = "";

	private FileType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
