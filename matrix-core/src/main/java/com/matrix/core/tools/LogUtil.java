package com.matrix.core.tools;

import org.apache.log4j.Logger;

/**
 * 日志工具
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月27日
 */
public class LogUtil {

	private static Logger log = Logger.getLogger(LogUtil.class);

	private static final int CALL_DEEP = 3;

	public static void info(String message, Object... params) {
		log.info(getInvokeMessage(formart(message, params)));
	}

	public static void debug(String message, Object... params) {
		log.debug(getInvokeMessage(formart(message, params)));
	}

	public static void warn(String message, Object... params) {
		log.warn(getInvokeMessage(formart(message, params)));
	}

	/**
	 * 打印error信息，带堆栈
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月27日
	 * @param message
	 * @param t
	 * @param params
	 */
	public static void error(String message, Throwable t, Object... params) {
		log.error(getInvokeMessage(formart(message, params)), t);
	}

	/**
	 * 打印error信息，不带堆栈
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月27日
	 * @param message
	 * @param params
	 */
	public static void error(String message, Object... params) {
		log.error(getInvokeMessage(formart(message, params)));
	}

	/**
	 * 构建类信息和方法信息
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月27日
	 * @param message
	 * @return
	 */
	public static String getInvokeMessage(String message) {
		StringBuffer msg = new StringBuffer();
		StackTraceElement[] track = Thread.currentThread().getStackTrace();
		if (track.length > CALL_DEEP && track[CALL_DEEP] != null) {
			StackTraceElement element = track[CALL_DEEP];
			msg.append(" [ ").append(element.getClassName()).append(".").append(element.getMethodName())
					.append("(" + element.getFileName() + ":" + element.getLineNumber() + ")").append(" ] — ");
		}
		return msg.append(message).toString();
	}

	/**
	 * 格式化参数
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月27日
	 * @param message
	 * @param params
	 * @return
	 */
	public static String formart(String message, Object... params) {

		message = message.replace("{}", "%s");
		if (params == null || params.length < 1) {
			return message;
		}
		return String.format(message, params);

	}

}
