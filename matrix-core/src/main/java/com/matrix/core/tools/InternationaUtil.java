package com.matrix.core.tools;

import java.util.Locale;

import org.springframework.context.MessageSource;

/**
 * 国际化信息工具
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月30日
 */
public class InternationaUtil {
	/**
	 * 获取语言环境中.propertoes中定义的信息
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月28日
	 * @param errorCode
	 * @param param
	 * @return
	 */
	public static String getMesssge(String code, Object... param) {
		MessageSource messageSource = (MessageSource) WebUtil.getApplicationContext().getBean("messageSource");
		return messageSource.getMessage(code, param, Locale.getDefault());
	}
}
