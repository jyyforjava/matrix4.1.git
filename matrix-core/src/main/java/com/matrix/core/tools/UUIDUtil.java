package com.matrix.core.tools;

import java.util.UUID;

/**
 * uuid工具
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月30日
 */
public class UUIDUtil {

	/**
	 * 获取一个32位的随机字符串
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @return
	 */
	public static String getRandomID() {
		return UUID.randomUUID().toString().replace("-", "");
	}
 
}
