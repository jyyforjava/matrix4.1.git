/**
 * 
 */
package com.matrix.core.anotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 在非数据库字段上加入注解，表示该字段不属于持久化字段
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date Dec 11, 2017
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Extend {

}
