package com.matrix.core.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.tools.InternationaUtil;

/**
 * @description Ajax请求返回的结果对象
 * @author 姜友瑶
 * @data 2016-06-26
 */
public class AjaxResult implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 请求成功
	 */
	public static final String STATUS_SUCCESS = "200";
	/** 请求未正常完成 **/
	public static final String STATUS_FAIL = SystemErrorCode.SYSTEM_ERROR_MSG;

	private String status;
	/**
	 * 请求后跳转的页面
	 */
	private String page;
	/** info会被国际化工具先处理，找不到国际化资源则显示原始信息 **/
	private String info;
	private Map<Object, Object> mapInfo = new HashMap<>();
	private List<?> rows;
	/**
	 * 总记录数
	 */
	private Integer total;

	public AjaxResult() {
	}

	public AjaxResult(String status, List<?> rows, Integer total) {
		this.status = status;
		this.rows = rows;
		this.total = total;
	}

	/**
	 * 设置简单信息，这是一个便捷的方法
	 * 
	 * @param status
	 * @param page
	 * @param info
	 */
	public AjaxResult(String status, List<?> rows) {
		this.status = status;
		this.rows = rows;
	}

	public AjaxResult(String status, String info, Object... param) {
		this.status = status;
		this.info = InternationaUtil.getMesssge(info, param);
	}
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Map<Object, Object> getMapInfo() {
		return mapInfo;
	}

	public void setMapInfo(Map<Object, Object> mapInfo) {
		this.mapInfo = mapInfo;
	}

	/**
	 * 
	 * @Description: 在map对象中放置信息
	 * @author:姜友瑶
	 * @param key
	 * @param value
	 *            返回类型 void
	 * @date 2016年9月11日
	 */
	public void putInMap(Object key, Object value) {
		mapInfo.put(key, value);
	}
}
