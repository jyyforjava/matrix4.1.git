package com.matrix.core.pojo;

import java.io.Serializable;
import java.util.Date;

import com.matrix.core.anotations.Extend;

import net.sf.json.JSONObject;

/**
 * bean对象的基类
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月30日
 */
public class EntityDTO  implements Serializable{

	@Extend
	private static final long serialVersionUID = 1L;

	private String createBy;

	private Date createTime;

	private String updateBy;

	private Date updateTime;

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		String objMsg = "[" + this.getClass().getSimpleName() + "]=" + JSONObject.fromObject(this).toString();
		return objMsg;
	}

}
