package com.matrix.core.web;

import java.util.List;

import com.matrix.core.pojo.PaginationVO;

/**
 * 
 * @Description: 所有业务service的基础类
 * @author:姜友瑶
 * @date 2016年8月28日
 */
public interface BaseServices<T> {

	/**
	 * 新增记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param obje
	 * @return
	 */
	public int add(T obje);

	/**
	 * 更新记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param oldValue
	 * @param newValue
	 * @return
	 */
	public int modifyByMap(T oldValue, T newValue);

	/**
	 * 批量删除记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param list
	 * @return
	 */
	public  int remove(List<String> list);

	/**
	 * 根据id删除记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param id
	 * @return
	 */
	public int removeById(String id);

	/**
	 * 分页查询记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param obje
	 * @param pageVo
	 * @return
	 */
	public List<T> findInPage(T obje, PaginationVO pageVo);

	/**
	 * 根据对象查询记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param obje
	 * @return
	 */
	public List<T> findByModel(T obje);

	/**
	 * 统计记录数记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param obje
	 * @return
	 */
	public int findTotal(T obje);

	/**
	 * 根据id查询记录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月29日
	 * @param id
	 * @return
	 */
	public T findById(String id);


}
