package com.matrix.core.web;

import java.util.List;

import com.matrix.core.constance.MatrixConstance;
import com.matrix.core.constance.SystemErrorCode;
import com.matrix.core.constance.SystemMessageCode;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.tools.InternationaUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.core.tools.WebUtil;

/**
 * @description 除了特殊的action一般业务action都继承这个action来实现 基本增删改查功能
 * @author 姜友瑶
 * @email 935090232@qq.com
 * @date 2016-06-26
 */
public abstract class BaseAction {

	/**
	 * 新增记录
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @param baseServices
	 * @param t
	 * @param name
	 * @return
	 */
	public <T> AjaxResult add(BaseServices<T> baseServices, T t, String name) {
		int i = baseServices.add(t);
		if (i > 0) {
			name=InternationaUtil.getMesssge(name);
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.ADD_SUCCES, name);
		} else {
			throw new GlobleException(SystemErrorCode.DATA_ADD_FAIL);
		}
	}

	/**
	 * 修改记录
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @param baseServices
	 * @param oldValue
	 * @param newValue
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> AjaxResult modify(BaseServices<T> baseServices, Object oldValue, T newValue, String name) {
		int i = baseServices.modifyByMap((T) oldValue, newValue);
		if (i > 0) {
			name=InternationaUtil.getMesssge(name);
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.UPDATE_SUCCES, name);
		} else {
			throw new GlobleException(SystemErrorCode.DATA_UPDATE_FAIL);
		}
	}

	/**
	 * 批量删除
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @param baseServices
	 * @param keys
	 * @return
	 */
	public <T> AjaxResult remove(BaseServices<T> baseServices, String keys) {
		List<String> ids = StringUtils.strToCollToString(keys, ",");
		int i = baseServices.remove(ids);
		if (i > 0) {
			return new AjaxResult(AjaxResult.STATUS_SUCCESS, SystemMessageCode.DELETE_SUCCES, i);
		} else {
			throw new GlobleException(SystemErrorCode.DATA_DELETE_FAIL);
		}
	}

	/**
	 * 分页查询
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @param baseServices
	 * @param t
	 * @param pageVo
	 * @return
	 */
	public <T> AjaxResult showList(BaseServices<T> baseServices, T t, PaginationVO pageVo) {
		if (pageVo == null) {
			pageVo = new PaginationVO();
		}
		List<T> dataList = baseServices.findInPage(t, pageVo);
		AjaxResult result = new AjaxResult(AjaxResult.STATUS_SUCCESS, dataList, baseServices.findTotal(t));
		return result;
	}

	/**
	 * 带条件查询，不分页
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @param baseServices
	 * @param t
	 * @return
	 */
	public <T> AjaxResult findByModel(BaseServices<T> baseServices, T t) {
		AjaxResult result = new AjaxResult(AjaxResult.STATUS_SUCCESS, baseServices.findByModel(t), 0);
		return result;
	}

	/**
	 * 根据id查询
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月30日
	 * @param baseServices
	 * @param id
	 * @return
	 */
	public <T> T findById(BaseServices<T> baseServices, String id) {
		return baseServices.findById(id);
	}

	/**
	 * 获取登录对象
	 * 
	 * @author 姜友瑶
	 * @date 2016/7/5
	 */
	public <T> T getSessionUser() {
		return WebUtil.getSessionAttribute(MatrixConstance.LOGIN_KEY);
	}

}
