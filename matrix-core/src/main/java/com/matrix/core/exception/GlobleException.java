package com.matrix.core.exception;

import com.matrix.core.tools.InternationaUtil;

/**
 * @description 系统异常类
 * @author 姜友瑶
 * @email 935090232@qq.com
 * @date 2016-06-26
 */
public class GlobleException extends RuntimeException {

	private static final long serialVersionUID = 5538900603076485646L;

	private String errorCode;

	/**
	 * 带堆栈信息
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月28日
	 * @param errorCode
	 * @param cause
	 * @param param
	 */
	public GlobleException(String errorCode, Throwable cause, Object... param) {
		super(getErrorMsg(errorCode, param), cause);
		this.errorCode = errorCode;

	}

	/**
	 * 不带堆栈
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月28日
	 * @param errorCode
	 * @param param
	 */
	public GlobleException(String errorCode, Object... param) {
		super(getErrorMsg(errorCode, param), null);
		this.errorCode = errorCode;

	}

	/**
	 * 不对堆栈，不带参数
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年11月28日
	 * @param errorCode
	 */
	public GlobleException(String errorCode) {
		super(getErrorMsg(errorCode), null);
		this.errorCode = errorCode;
	}

	private static String getErrorMsg(String errorCode, Object... param) {
		String message = InternationaUtil.getMesssge(errorCode, param);
		return message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	
	

}
