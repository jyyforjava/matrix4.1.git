package com.matrix.async.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.matrix.async.bean.AsyncLockBean;
import com.matrix.async.bean.AsyncTaskBean;
import com.matrix.async.bean.AsyncTaskDataBean;
import com.matrix.async.bean.AsyncTaskPackageBean;
import com.matrix.async.core.AsyncTask;
import com.matrix.core.pojo.PaginationVO;

public interface AsyncTaskDao {

	void insertTaskPackage(AsyncTaskPackageBean taskPackage);

	void insertTask(AsyncTaskBean task);

	void batchInsertTaskData(List<AsyncTaskDataBean> dataList);

	int selectCountAsyncLock(String taskType);

	void insertAsyncyLock(AsyncLockBean asyncLockBean);

	void locokAsyncTask(String taskType);

	void updateTaskPackageBatchNo(@Param("batchNo") String batchNo, @Param("taskType") String taskType,
			@Param("batchSize") int batchSize, @Param("hostName") String hostName);

	List<String> slectPackageIdList(String batchNo);

	void updateAsycnLock(@Param("lockedTiem") Date lockedTiem, @Param("getTaskTime") Date getTaskTime,
			@Param("taskType") String taskType);

	void updateAsyncTaskBatchNo(String batchNo);

	List<String> selectTaskByPackageId(String packageId);

	List<AsyncTaskDataBean> selectTaskDataList(String taskId);

	void backupTaskPackage(String packageId);

	void deleteTaskPackageById(String packageId);

	void updateTaskInfo(AsyncTaskBean asyncTaskBean);

	AsyncTaskBean selectTaskByTaskId(String id);

	void backupTask(@Param("taskId") String taskId, @Param("tableName") String tableName);

	void backupTaskData(@Param("taskId") String taskId, @Param("tableName") String tableName);

	void deleteTaskById(String taskId);

	void deleteTaskDateByTaskId(String taskId);

	List<String> selectOvertimePackageIdList(@Param("hostName") String hostName,@Param("overTime")String overTime);

	void recoverTaskPackage(List<String> packageIdList);

	void recoverTask(List<String> packageIdList);

	List<AsyncTaskBean> selectInPage(@Param("record") AsyncTaskBean task, @Param("pageVo")PaginationVO pageVo);

	Integer selectTotalRecord(@Param("record")AsyncTaskBean task);

	List<String> callPackageIds(@Param("taskType")String taskType, @Param("hostName")String hostName, @Param("batchSize")int batchSize
			,@Param("batchNo")String batchNo);

}
