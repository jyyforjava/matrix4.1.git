package com.matrix.async.action;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.async.bean.AsyncTaskBean;
import com.matrix.async.dao.AsyncTaskDao;
import com.matrix.async.service.AsyncTaskService;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.web.BaseAction;

@Controller
@RequestMapping(value = "admin/async")
public class AsyncTaskAction extends BaseAction {

	@Autowired
	private AsyncTaskService asyncTaskService;

	@Autowired
	private AsyncTaskDao asyncTaskDao;

	/**
	 * 进入编辑界面
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年6月12日
	 * @return
	 */
	@RequestMapping(value = "/manageAsync")
	public String manageAsync() {
		return "async/async-list";
	}

	@RequestMapping(value = "/del/{taskId}/{packageId}")
	public @ResponseBody AjaxResult del(@PathVariable("taskId") String taskId,
			@PathVariable("packageId") String packageId) {
		asyncTaskDao.deleteTaskById(taskId);
		asyncTaskDao.deleteTaskPackageById(packageId);
		asyncTaskDao.deleteTaskDateByTaskId(taskId);
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "删除成功");
	}

	@RequestMapping(value = "/copy/{taskId}")
	public @ResponseBody AjaxResult copy(@PathVariable("taskId") String taskId) {
		AsyncTaskBean task = asyncTaskDao.selectTaskByTaskId(taskId);
		asyncTaskService.createTask("asyncControlCopy", task.getTaskType(), asyncTaskService.getTaskDataList(taskId));
		return new AjaxResult(AjaxResult.STATUS_SUCCESS, "复制成功");
	}

	/**
	 * 查询运行中的异步任务
	 */
	@RequestMapping(value = "/showList")
	public @ResponseBody AjaxResult showList(AsyncTaskBean task, PaginationVO pageVo) {
		if (pageVo == null) {
			pageVo = new PaginationVO();
		}
		List<AsyncTaskBean> dataList = asyncTaskService.findInPage(task, pageVo);
		AjaxResult result = new AjaxResult(AjaxResult.STATUS_SUCCESS, dataList, asyncTaskService.findTotal(task));
		return result;
	}

}