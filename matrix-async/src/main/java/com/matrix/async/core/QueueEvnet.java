package com.matrix.async.core;

public class QueueEvnet extends APPEvent {

	private static final long serialVersionUID = 2807610129599099531L;

	private Object element = null;

	public QueueEvnet(APPQueue source, Object element) {
		this(source, element, System.currentTimeMillis());
	}

	public QueueEvnet(APPQueue source, Object element, long when) {
		super(source, when);
		this.element = element;
	}

	public Object getElement() {
		return element;
	}

}
