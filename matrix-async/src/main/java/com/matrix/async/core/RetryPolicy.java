package com.matrix.async.core;

/**
 * 重试策略
 * 
 * @author jiangyouyao
 *
 */
public interface RetryPolicy {
	
	boolean isRetry();
	 
}
