package com.matrix.async.core;

/**
 * 可控制线程
 * 
 * @author jiangyouyao
 *
 */
public interface Controllable {
	// 初始化
	static final int INIT = 0;
	// 运行中
	static final int RUNNING = 1;
	// 终止
	static final int SHUTDIWN = 2;
	// 暂停
	static final int SUSPEND = 3;
	// 结束
	static final int TERMINATED = 4;

	void start();

	void suspend();

	/**
	 * 唤醒
	 */
	void resume();

	void shutdownNow();

	void shutdown();
}
