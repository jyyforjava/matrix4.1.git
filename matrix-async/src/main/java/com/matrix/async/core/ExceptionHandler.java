package com.matrix.async.core;

import org.apache.log4j.Logger;

import com.matrix.async.service.AsyncTaskService;

public class ExceptionHandler extends WorkThread {
	private Logger log = Logger.getLogger(ExceptionHandler.class);
	private AsyncTaskService asyncTaskService;
	private int interval = 60 * 60 *1000; //每一个小时启动一次
	private int overtime = 60 * 60; //单位是秒

	ExceptionHandler() {
	}

	ExceptionHandler(AsyncTaskService asyncTaskService, ThreadFactory threadFactory) {
		this.name = "ExcetionHandler";
		this.asyncTaskService = asyncTaskService;
		this.thread = (threadFactory != null ? threadFactory.newThread(name, this)
				: WorkThread.defaultThreadFactory().newThread(name, this));
	}

	protected void work() {
		recoverOvertimeTask();
		try {
			Thread.sleep(interval);
		} catch (InterruptedException e) {
		}
	}

	private void recoverOvertimeTask() {
		try {
			log.info("清理长期处理待处理的任务 ********|||*******");
			asyncTaskService.recoverOvertimeTask(String.valueOf(overtime));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public void setOvertime(int overtime) {
		this.overtime = overtime;
	}

}
