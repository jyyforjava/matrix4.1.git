package com.matrix.async.core;

import java.io.Serializable;

public interface Task extends Serializable, APPComponent, QueueListener {

	String getID();

	void beforeExecute();

	boolean execute();

	void afterExecute();

	/**
	 * 是否重试
	 * 
	 * @return
	 */
	boolean retry();

	/**
	 * 任务异常回调
	 * 
	 * @param e
	 */
	void onError(Exception e);

	/**
	 * 设置重试策略
	 */
	void setRetryPolicy(RetryPolicy retryPolicy);

}
