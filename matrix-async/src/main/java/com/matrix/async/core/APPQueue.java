package com.matrix.async.core;

import java.util.List;

/**
 * 应用队列接口
 * @author jiangyouyao
 *
 */
public interface APPQueue extends APPComponent{
	void put(Task task);
	
	public void put(List<Task> tasks);
	
	public Task poll();
	
	public Task take() throws InterruptedException;
	
	int size();
	
}
