package com.matrix.async.core;

public interface ThreadFactory {

	Thread newThread(Runnable r);

	Thread newThread(String namePrefix, Runnable r);

}
