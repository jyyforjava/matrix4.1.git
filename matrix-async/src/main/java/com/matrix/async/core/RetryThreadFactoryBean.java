package com.matrix.async.core;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * 异步任务重试机制
 * 
 * @author jiangyouyao
 */
public class RetryThreadFactoryBean implements InitializingBean, FactoryBean<Object> {

	private String taskType = null;
	private DataSource dataSource = null;
	private String packageName = null;
	private int startuopDelay = 0;
	private int DEFAULT_RETRY_INTERVAL = 5 * 60 * 1000;
	private int retryInterval = DEFAULT_RETRY_INTERVAL;

	private RetryThread thread = null;

	@Override
	public void afterPropertiesSet() throws Exception {
		if (taskType == null) {
			throw new IllegalArgumentException("The parameter 'taskType' is necessary");
		}
		if (dataSource == null) {
			throw new IllegalArgumentException("The parameter 'dataSource' is necessary");
		}

		if (packageName == null) {
			throw new IllegalArgumentException("The parameter 'packageName' is necessary");
		}

		thread = new RetryThread(taskType, dataSource, packageName);

		thread.setStartupDelay(startuopDelay);
		thread.setRetryInterval(retryInterval);

	}

	@Override
	public Object getObject() throws Exception {

		return thread;
	}

	@Override
	public Class<? extends RetryThreadFactoryBean> getObjectType() {
		return this.getClass();
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public void setStartuopDelay(int startuopDelay) {
		this.startuopDelay = startuopDelay;
	}

	public void setRetryInterval(int retryInterval) {
		this.retryInterval = retryInterval;
	}

}
