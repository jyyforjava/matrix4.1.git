package com.matrix.async.core;

import java.io.Serializable;
import java.util.Map;

public interface TaskHandler extends Serializable{

	boolean execute(Map<String, String> dataMap);

	void onError(Map<String, String> dataMap, Exception e);

}
