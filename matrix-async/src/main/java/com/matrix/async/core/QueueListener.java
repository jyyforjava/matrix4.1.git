package com.matrix.async.core;

import java.util.EventListener;

/**
 * 队列事件监听
 * 
 * @author jiangyouyao
 *
 */
public interface QueueListener extends EventListener {

	/**
	 * 入栈事件
	 * 
	 * @param e
	 */
	void enqueue(QueueEvnet e);

	/**
	 * 出栈事件
	 * 
	 * @param e
	 */
	void dequeue(QueueEvnet e);

	/**
	 * 队列工作不饱和
	 */
	void lackOfTask(QueueEvnet e);

	/**
	 * 队列拥挤
	 * 
	 * @param e
	 */
	void fullOfTask(QueueEvnet e);

}
