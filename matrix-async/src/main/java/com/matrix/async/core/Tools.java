package com.matrix.async.core;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Tools {
	private static final String dataformat = "yyyy-MM-dd HH:mm:ss";

	public static String getDateString(Date date) {
		if (date != null) {
			DateFormat fmt = new SimpleDateFormat(dataformat);
			return fmt.format(date);
		} else {
			return "null";
		}

	}

	public static Object getDateString(Date date, String string) {
		if (date != null) {
			DateFormat fmt = new SimpleDateFormat(string);
			return fmt.format(date);
		} else {
			return "null";
		}
	}

	/**
	 * 获取一个32位的随机数
	 * 
	 * @return
	 */
	public static String createUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
	}

	public static String getHostName() {
		String hostName = "";
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
		}
		return hostName;
	}
}
