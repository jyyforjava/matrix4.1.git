package com.matrix.async.core;

import java.util.ArrayList;
import java.util.List;

/**
 * 一组任务，包含消费者，生产者，异常处理
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年9月4日
 */
public class APPBundle {
	private final String name;
	private APPQueue taskQueue = null;
	private Producer producer = null;
	private List<Comsumer> consumers = new ArrayList<>();
	private ExceptionHandler exceptionHandler = null;

	public APPBundle(String name) {
		this.name = name;
	}

	public APPQueue getTaskQueue() {
		return taskQueue;
	}

	public void setTaskQueue(APPQueue taskQueue) {
		this.taskQueue = taskQueue;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public List<Comsumer> getConsumers() {
		return consumers;
	}

	public void setConsumers(List<Comsumer> consumers) {
		this.consumers = consumers;
	}

	public ExceptionHandler getExceptionHandler() {
		return exceptionHandler;
	}

	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}

	public RetryThread getRetryThread() {
		return retryThread;
	}

	public void setRetryThread(RetryThread retryThread) {
		this.retryThread = retryThread;
	}

	public String getName() {
		return name;
	}

	private RetryThread retryThread = null;

	public void addConsumers(Comsumer consumer) {
		this.consumers.add(consumer);

	}

}
