package com.matrix.async.core;

public interface APPComponent {

	/**
	 * 获取组件名称
	 * 
	 * @return
	 */
	String getName();

	/**
	 * 获取任务类型
	 * 
	 * @return
	 */
	String getTaskType();


}
