package com.matrix.async.core;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class Retry3Times implements RetryPolicy, Serializable {

	private static final long serialVersionUID = -880008704755721317L;

	private AtomicInteger retryTimes = new AtomicInteger(0);

	@Override
	public boolean isRetry() {
		return retryTimes.getAndIncrement() < 3;
	}
 
}
