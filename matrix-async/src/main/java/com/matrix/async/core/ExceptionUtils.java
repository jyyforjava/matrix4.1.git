package com.matrix.async.core;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {

	/**
	 * 获取异常堆栈信息
	 * @param e
	 * @return
	 */
	public static String getStackTraceMessage(Exception e) {
		if (e == null) {
			return null;
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

}
