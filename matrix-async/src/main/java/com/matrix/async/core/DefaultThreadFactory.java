package com.matrix.async.core;

public class DefaultThreadFactory implements ThreadFactory {

	private ThreadGroup group = null;
	private int priority = Thread.NORM_PRIORITY;

	public DefaultThreadFactory() {
		SecurityManager s = System.getSecurityManager();
		group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
	}

	@Override
	public Thread newThread(Runnable r) {

		return newThread("WorkTHread", r);
	}

	@Override
	public Thread newThread(String namePrefix, Runnable r) {
		Thread t = new Thread(group, r, namePrefix, 0);
		t.setDaemon(false);
		t.setPriority(priority);
		return t;
	}

	public void setGroup(ThreadGroup group) {
		this.group = group;
	}

}
