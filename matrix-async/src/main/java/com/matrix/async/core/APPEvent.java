package com.matrix.async.core;

import java.util.EventObject;

/**
 * 异步组件事件
 * 
 * @author jiangyouyao
 *
 */
public class APPEvent extends EventObject {

	private static final long serialVersionUID = 1L;
	private long when = 0L;

	public APPEvent(Object source) {
		super(source);
	}

	public APPEvent(APPQueue source, long when) {
		super(source);
		this.when = when;
	}

	long getWhen() {
		return when;
	}

}
