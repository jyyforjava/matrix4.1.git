package com.matrix.async.core;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BundleRepository {

	private Map<String, APPBundle> repository = null;

	public BundleRepository() {
		repository = new HashMap<>();
	}

	public synchronized boolean bind(APPBundle bundle) {
		return repository.put(bundle.getName(), bundle) != null;
	}

	public synchronized boolean remove(String name) {
		return repository.remove(name) != null;
	}

	public synchronized APPBundle lookup(String name) {
		return repository.get(name);
	}

	public synchronized Collection<APPBundle> lookupAll() {
		return Collections.unmodifiableCollection(repository.values());
	}

}
