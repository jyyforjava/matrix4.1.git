package com.matrix.async.core;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ComponentRepository {

	private Map<String, APPComponent> repository = null;

	public ComponentRepository() {
		repository = new HashMap<String, APPComponent>();
	}

	public synchronized boolean bind(APPComponent compoment) {
		if (repository.get(compoment.getName()) != null) {
			throw new RuntimeException(" APPCompoment with name " + compoment.getName() + " is already exists");
		}
		return repository.put(compoment.getName(), compoment) != null;
	}

	public synchronized boolean remove(String name) {
		return repository.remove(name) != null;
	}

	public synchronized APPComponent lockup(String name) {
		return repository.get(name);
	}

	public synchronized Collection<APPComponent> lockupAll() {
		return Collections.unmodifiableCollection(repository.values());
	}

}
