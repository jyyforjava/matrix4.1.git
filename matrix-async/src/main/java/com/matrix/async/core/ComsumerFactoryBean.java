package com.matrix.async.core;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public class ComsumerFactoryBean implements InitializingBean, FactoryBean<Object> {

	public static final Map<String, Boolean> comsumerThreadNameMap = new HashMap<>();

	private String taskType = null;

	private APPQueue taskQueue = null;
	private ThreadFactory threadFactory = null;
	private int startupDelay = 0;
	private String beanUse = "false";
	private Comsumer thread = null;

	public ComsumerFactoryBean() {
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (taskType == null) {
			throw new IllegalArgumentException("The parameter 'taskType' is necessary");
		}
		if (taskQueue == null) {
			throw new IllegalArgumentException("The parameter 'taskQueue' is necessary");
		}

		thread = new Comsumer(taskType, taskQueue, threadFactory);

		comsumerThreadNameMap.put(thread.getName(), Boolean.TRUE);

		thread.setStartupDelay(startupDelay);
		thread.setBeanUse(beanUse);

	}


	public Object getObject() throws Exception {
		return thread;
	}

	@Override
	public Class<?> getObjectType() {
		return Comsumer.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}
	
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public void setTaskQueue(APPQueue taskQueue) {
		this.taskQueue = taskQueue;
	}
	

	public void setThreadFactory(ThreadFactory threadFactory) {
		this.threadFactory = threadFactory;
	}

	public void setStartupDelay(int startupDelay) {
		if (startupDelay <= 0) {
			throw new IllegalArgumentException("The paramenter 'startupDelay' can't be less than 1 ");
		}
		this.startupDelay = startupDelay;
	}
	
	public void setBeanUse(String beanUse) {
		this.beanUse = beanUse;
	}
	
	


}
