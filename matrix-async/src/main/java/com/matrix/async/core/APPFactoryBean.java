package com.matrix.async.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public class APPFactoryBean implements InitializingBean, FactoryBean<Object> {

	private Logger log = Logger.getLogger(APPFactoryBean.class);

	private List<APPComponent> componentList = new ArrayList<>();
	private String beanUse = "false";

	private AsyncProcessPlatform app = AsyncProcessPlatform.getInstance();

	public APPFactoryBean() {

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if ("false".equals(beanUse)) {
			log.info("异步平台不启动");
			return;
		}
		for (APPComponent component : componentList) {
			app.bindComponent(component);
			app.bundleComponent(component);
		}

		app.start();
		log.info("异步启动!!!");

	}

	@Override
	public Object getObject() throws Exception {
		return app;
	}

	@Override
	public Class<?> getObjectType() {
		return AsyncProcessPlatform.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setComponentList(List<APPComponent> componentList) {
		this.componentList = componentList;
	}

	public void setBeanUse(String beanUse) {
		this.beanUse = beanUse;
	}

	public void destroy() {
		app.shutdown();
	}

}
