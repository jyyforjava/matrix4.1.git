package com.matrix.async.core;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public class TaskQueueFactoryBean implements InitializingBean, FactoryBean<Object> {

	private String taskType = null;

	private int upperBoundary = 1000;
	private int lowerBoundary = 10;

	private APPQueue queue = null;

	@SuppressWarnings("unused")
	private String beanUse = "false";

	public TaskQueueFactoryBean() {
	}

	@Override
	public Object getObject() throws Exception {
		return queue;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (taskType == null) {
			throw new IllegalArgumentException("The parameter 'taskType' is necessary");
		}
		if (upperBoundary < lowerBoundary) {
			throw new IllegalArgumentException("The parameter 'upperBoundary' can't be less than 'lowerBoundary' ");
		}

		queue = new TaskQueue(taskType, upperBoundary, lowerBoundary);

	}

	@Override
	public Class<?> getObjectType() {
		return TaskQueue.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public void setUpperBoundary(int upperBoundary) {
		if (upperBoundary < 0) {
			throw new IllegalArgumentException(
					"The parameter 'upperBoundary' can't less than 0 or more than " + Integer.MAX_VALUE);
		}
		this.upperBoundary = upperBoundary;
	}

	public void setLowerBoundary(int lowerBoundary) {

		if (lowerBoundary < 0) {
			throw new IllegalArgumentException(
					"The parameter 'lowerBoundary' can't less than 0 or more than " + Integer.MAX_VALUE);
		}
		this.lowerBoundary = lowerBoundary;
	}

	public void setBeanUse(String beanUse) {
		this.beanUse = beanUse;
	}

}
