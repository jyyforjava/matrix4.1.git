package com.matrix.async.service;

import java.util.List;
import java.util.Map;

import com.matrix.async.bean.AsyncTaskBean;
import com.matrix.async.core.AsyncTask;
import com.matrix.core.pojo.PaginationVO;
import com.matrix.core.web.BaseServices;

public interface AsyncTaskService {

	/**
	 * 生成异步任务
	 * 
	 * @return
	 */
	void createTask(String crearedBy, String taskType, Map<String, String> dataMap);

	/**
	 * 查询任务包id
	 * 
	 * @param taskType
	 * @param batchSize
	 * @return
	 */
	List<String> getTaskPackageIdList(String taskType, int batchSize);

	/**
	 * 
	 * @param packageId
	 * @return
	 */
	List<String> getTaskIdList(String packageId);

	/**
	 * 获取任务的数据
	 * 
	 * @param taskId
	 * @return
	 */
	Map<String, String> getTaskDataList(String taskId);

	/**
	 * 把执行成功的任务包写入成功表
	 * 
	 * @param map
	 */
	void backupTaskPackage(String packageId);

	/**
	 * 将超过配置时间还一直处于处理中
	 * 
	 * @param valueOf
	 */
	void recoverOvertimeTask(String valueOf);

	void updateTaskInfo(AsyncTaskBean asyncTaskBean);

	List<AsyncTaskBean> findInPage(AsyncTaskBean task, PaginationVO pageVo);

	Integer findTotal(AsyncTaskBean task);

}
