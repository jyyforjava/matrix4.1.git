package com.matrix.async.handle;

import java.util.Map;

import org.apache.log4j.Logger;

import com.matrix.async.core.TaskHandler;

public class TestHandler1 implements TaskHandler {
	private static final long serialVersionUID = -1474627493567922828L;
	private Logger log = Logger.getLogger(TestHandler1.class);

	@Override
	public boolean execute(Map<String, String> dataMap) {
		
		log.info("短信发送---------》》》》》》》》》 执行成功");
		try {
			Thread.sleep(100000000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void onError(Map<String, String> dataMap, Exception e) {
		log.info("task handle onError ");

	}

}
