package com.matrix.async.handle;

import java.util.Map;

import com.matrix.async.core.TaskHandler;

public class TestHandler implements TaskHandler {
	private static final long serialVersionUID = -1474627493567922828L;

	@Override
	public boolean execute(Map<String, String> dataMap) {
		System.out.println("邮件发送");
		try {
			Thread.sleep(1000000000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void onError(Map<String, String> dataMap, Exception e) {
	}
}
