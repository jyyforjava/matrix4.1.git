package com.matrix.async.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.matrix.async.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;

import com.matrix.async.core.Tools;
import com.matrix.async.service.AsyncTaskService;

public class AsyncTaskServiceImplTest2 extends BaseTest {

	@Autowired
	AsyncTaskService asyncTaskService;
	private String taskType2 = "邮件发送";

	@Test
	public void testCreateTask() {
		//在数据库中创建一个异步任务
		asyncTaskService.createTask("姜友瑶", taskType2, getDataMap());
	}

	//随机产生几个参数，这些参数可以在任务处理类中获取
	private Map<String, String> getDataMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("参数1", Tools.createUUID());
		map.put("参数2", Tools.createUUID());
		map.put("参数3", Tools.createUUID());
		return map;
	}
}
