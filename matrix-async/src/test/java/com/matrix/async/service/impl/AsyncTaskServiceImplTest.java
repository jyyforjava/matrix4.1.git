package com.matrix.async.service.impl;

import static org.junit.Assert.fail;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.matrix.async.BaseTest;
import org.springframework.beans.factory.annotation.Autowired;

import com.matrix.async.bean.AsyncTaskBean;
import com.matrix.async.core.AsyncTask;
import com.matrix.async.core.Tools;
import com.matrix.async.service.AsyncTaskService;
import com.matrix.core.pojo.PaginationVO;

public class AsyncTaskServiceImplTest extends BaseTest {

	@Autowired
	AsyncTaskService asyncTaskService;
	private String taskType = "邮件发送";
	private String taskType1 = "邮件发送";
	private String taskType2 = "短信发送";

	@Test
	public void testCreateTask() {

		for (int i = 0; i < 10; i++) {
			if (i % 2 == 0) {
				asyncTaskService.createTask("姜友瑶", taskType1, getDataMap());
			} else {
				asyncTaskService.createTask("姜友瑶", taskType2, getDataMap());
			}
			System.out.println("==========================" + i);
		}

	}

	private Map<String, String> getDataMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("参数1", Tools.createUUID());
		map.put("参数2", Tools.createUUID());
		map.put("参数3", Tools.createUUID());
		return map;
	}

	@Test
	public void testGetTaskPackageIdList() {
		Assert.assertEquals(asyncTaskService.getTaskPackageIdList(taskType1, 10).size(), 1);
	}

	@Test
	public void testGetTaskIdList() {
		Assert.assertEquals(asyncTaskService.getTaskIdList("610627821CC9498B93D06FCDD38B14BB").size(), 1);
	}

	@Test
	public void testGetTaskDataList() {
		Assert.assertEquals(asyncTaskService.getTaskDataList("C3C2223C8B594B848A2C2ED53405CF17").size(), 3);
	}

	@Test
	public void testBackupTaskPackage() {
		asyncTaskService.backupTaskPackage("610627821CC9498B93D06FCDD38B14BB");
	}

	@Test
	public void testUpdateTaskInfo() {
		AsyncTaskBean taskBean = new AsyncTaskBean();
		taskBean.setId("0003CD72A125459C96B6B7CC3835AAD1");
		taskBean.setStatus(2 + "");
		taskBean.setProcessedBy("----");
		taskBean.setEnqueueTime(new Date());
		taskBean.setStartTime(new Date());
		taskBean.setFinishTime(new Date());
		taskBean.setErrorLog("error");
		asyncTaskService.updateTaskInfo(taskBean);
	}

	@Test
	public void testRecoverOvertimeTask() {

		asyncTaskService.recoverOvertimeTask(600 + "");

	}
 

}
