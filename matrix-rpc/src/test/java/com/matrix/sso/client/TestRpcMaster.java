package com.matrix.sso.client;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.Test;

import com.matrix.rpc.service.RegistryService;

public class TestRpcMaster {

	@Test
	public void test() throws Exception {
		Server server=new Server(12580);
		ServletContextHandler context=new ServletContextHandler(server,"/");
		server.setHandler(context);
		context.addServlet(RegistryService.class,"/rpcmaster");
		server.start();
		server.join();
	}

}
