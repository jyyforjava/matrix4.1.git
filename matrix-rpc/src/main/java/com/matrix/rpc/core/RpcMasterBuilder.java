package com.matrix.rpc.core;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.springframework.beans.factory.InitializingBean;

import com.matrix.core.tools.LogUtil;
import com.matrix.rpc.service.ProvideUrlService;
import com.matrix.rpc.service.RegistryService;
import static com.matrix.rpc.core.RpcConstant.*;

public class RpcMasterBuilder implements InitializingBean {

	private Integer point;

	@Override
	public void afterPropertiesSet() throws Exception {
		if (point == null) {
			throw new IllegalArgumentException("The parameter 'point' is necessary");
		}
		// 新建服务
		Server server = new Server(point);

		ServletContextHandler context = new ServletContextHandler(server, "/");
		server.setHandler(context);
		// 注册服务
		context.addServlet(RegistryService.class, "/rpc/registry");
		context.addServlet(ProvideUrlService.class, URI_SERVICE_PROVIDE);
		// 启动监听
		server.start();

		LogUtil.info("Matrix RPC master  start success listen in {}", point);
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

}
