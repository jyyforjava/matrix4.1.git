package com.matrix.rpc.core;

import java.util.List;

/**
 * 远程服务类
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月23日
 */
public class RpcService {

	 

	private ServiceStartegy strategy;

	private String serviceName;

	private List<IP> ipList;

	public ServiceStartegy getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) { 
		this.strategy = ServiceStartegy.getServiceStartegyByName(strategy);
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public List<IP> getIpList() {
		return ipList;
	}

	public void setIpList(List<IP> ipList) {
		this.ipList = ipList;
	}

}
