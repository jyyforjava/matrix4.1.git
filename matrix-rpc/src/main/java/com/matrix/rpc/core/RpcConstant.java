package com.matrix.rpc.core;

public class RpcConstant {
	/**
	 * 服务注册uri
	 */
	public static final String URI_REGISTRY = "/rpc/registry";
	/**
	 * 服务获取uri
	 */
	public static final String URI_SERVICE_PROVIDE = "/rpc/serviceProvide";
	
	/**
	 * 服务执行uri
	 */
	public static final String URI_SERVICE_EXECUTE = "/rpc/serviceExcute";
	
	
	/**
	 * 服务不存在
	 */
	public static final String SERVICE_NON_EXISTENT = "0";
	
	/**
	 * 服务存在
	 */
	public static final String SERVICE_EXISTENT = "1";
	/**
	 * 获取服务失败
	 */
	public static final String SERVICE_ERROR = "2";
}
