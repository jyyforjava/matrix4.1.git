package com.matrix.rpc.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matrix.core.tools.LogUtil;

public class ServicesMapping {

	static Map<String, RpcService> servicesMapping = new HashMap<>();

	/**
	 * 根据服务名称获取服务
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月24日
	 * @param serviceName
	 * @return
	 */
	public static RpcService getRpcServiceByName(String serviceName) {
		return servicesMapping.get(serviceName);
	}

	/**
	 * 注册服务
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月23日
	 * @param serviceName
	 * @param ip
	 * @param strategy
	 * @param weight
	 * @return
	 */
	public static boolean registerService(String serviceName, String ip, String strategy, int weight) {
		RpcService rpcService = servicesMapping.get(serviceName);
		IP rpcIp = new IP();
		rpcIp.setIpAddress(ip);
		rpcIp.setWeight(weight);
		if (rpcService == null) {
			rpcService = new RpcService();
			rpcService.setServiceName(serviceName);
			rpcService.setStrategy(strategy);
			List<IP> ipList = new ArrayList<>();
			ipList.add(rpcIp);
			rpcService.setIpList(ipList);
			LogUtil.info("注册服务{},注册IP地址{},权重{} 当前在线ip{}", serviceName, ip, weight, ipList.toString());
			servicesMapping.put(serviceName, rpcService);
		} else {
			// 判断ip地址是否存在
			List<IP> ips = rpcService.getIpList();
			if (!ips.contains(rpcIp)) {
				// 如果没有包含ip，则添加
				ips.add(rpcIp);
				LogUtil.info("追加服务{} IP地址{},权重{} 当前在线ip{}", serviceName, ip, weight, ips.toString());
			} else {
				LogUtil.info("重复注册服务{} IP地址{},权重{} 当前在线ip{}", serviceName, ip, weight, ips.toString());
				return false;
			}
		}
		return true;
	}

}
