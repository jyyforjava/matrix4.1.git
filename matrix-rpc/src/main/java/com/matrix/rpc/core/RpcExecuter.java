package com.matrix.rpc.core;

import java.util.Map;
/**
 * 远程执行类
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月24日
 */
public interface RpcExecuter {
	public Map<String, String> execute(Map<String, String> param);
}
