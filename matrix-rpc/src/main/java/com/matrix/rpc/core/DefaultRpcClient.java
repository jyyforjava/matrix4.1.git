package com.matrix.rpc.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.StringUtils;

@Component
public class DefaultRpcClient implements RpcClient {

	@Autowired
	ServiceRegistryBuilder serviceRegistryBuilder;

	// 数组角标，记录轮询
	private Map<String, Integer> posMap = new HashMap<>();

	@Override
	public Object requestSliceService(String serviceName, Map<String, String> param) {
		LogUtil.debug("开始调用分片远程服务" + serviceName);

		// 获取远程服务对象
		RpcService rpcService = getRpcService(serviceName);

		List<IP> ipList = rpcService.getIpList();
		List<JSONObject> resultList = new ArrayList<JSONObject>();
		LogUtil.debug("分片总数={}" +  ipList.size());
		for (int i = 0; i < ipList.size(); i++) {
			JSONObject json = new JSONObject();
			param.put("_currentIndex", i + "");
			param.put("_sliceTotal", ipList.size() + "");
			String result = requestRpcServiceResult(serviceName, param, ipList.get(i).getIpAddress());
			json.put("result", result);
			json.put("_currentIndex", i + "");
			json.put("_sliceTotal", ipList.size() + "");
			resultList.add(json);
		}
		return resultList;
	}

	@Override
	public Object requestService(String serviceName, Map<String, String> paramMap) {
		LogUtil.debug("开始调用远程服务" + serviceName);

		// 获取远程服务对象
		RpcService rpcService = getRpcService(serviceName);
		// 执行远程服务
		if (rpcService != null) {
			ServiceStartegy serviceStartegy = rpcService.getStrategy();
			LogUtil.info("获取到服务{}的策略={}", serviceName, serviceStartegy);
			String ipAddress = "";
			switch (serviceStartegy) {
			case RANDOM:
				ipAddress = getRandomIp(rpcService);
			case ROTATION:
				ipAddress = getRotationIp(rpcService);
				break;
			case WEIGHT:
				ipAddress = getWeightIp(rpcService);
				break;
			}
			if (StringUtils.isNotBlank(ipAddress)) {
				return requestRpcServiceResult(serviceName, paramMap, ipAddress);
			} else {
				LogUtil.error("获取远程ip失败 ipAddress is null");
				throw new GlobleException("获取远程ip失败 ipAddress is null");
			}

		} else {
			LogUtil.error("获取远程服务列表失败 rpcService is null");
			throw new GlobleException("获取远程服务列表失败");
		}
	}

	/**
	 * 加权随机获取ip
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月27日
	 * @param rpcService
	 * @return
	 */
	private String getWeightIp(RpcService rpcService) {
		List<IP> ipList = rpcService.getIpList();
		Random random = new Random();
		List<IP> ipListNew = new ArrayList<>();
		for (IP ip : ipList) {
			for (int i = 0; i > ip.getWeight(); i++) {
				ipListNew.add(ip);
			}
		}
		int randomPos = random.nextInt(ipList.size());
		return ipListNew.get(randomPos).getIpAddress();
	}

	/**
	 * 获取轮询ip
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月27日
	 * @param rpcService
	 * @return
	 */
	private synchronized String getRotationIp(RpcService rpcService) {
		Integer pos = posMap.get(rpcService.getServiceName());
		if (pos == null) {
			pos = 0;
			posMap.put(rpcService.getServiceName(), pos);
		}
		List<IP> ipList = rpcService.getIpList();
		IP ip = ipList.get(pos);
		String ipAddress = ip.getIpAddress();
		pos = pos + 1;
		return ipAddress;
	}

	/**
	 * 对远程服务发送http请求并获取返回结果
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月27日
	 * @param serviceName
	 * @param paramMap
	 * @param ipAddress
	 * @return
	 */
	private String requestRpcServiceResult(String serviceName, Map<String, String> paramMap, String ipAddress) {
		try {
			List<NameValuePair> _params = new ArrayList<NameValuePair>();
			// 构建请求参数
			Set<String> paramSet = paramMap.keySet();
			for (String key : paramSet) {
				_params.add(new BasicNameValuePair(key, paramMap.get(key)));
			}
			_params.add(new BasicNameValuePair("serviceName", serviceName));
			HttpPost _httpPost = new HttpPost(ipAddress + RpcConstant.URI_SERVICE_EXECUTE);
			_httpPost.setEntity(new UrlEncodedFormEntity(_params));

			LogUtil.info("请求远程服 地址={} 请求参数={}", ipAddress + RpcConstant.URI_SERVICE_EXECUTE, JSON.toJSON(_params));
			HttpClient httpclient = HttpClients.createDefault();
			HttpResponse _response = httpclient.execute(_httpPost);

			if (_response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String _result = EntityUtils.toString(_response.getEntity(), "utf-8");
				LogUtil.debug("获取服务结果=" + _result);
				return _result;
			} else {
				LogUtil.error("请求远程服务错误 状态码={}", _response.getStatusLine().getStatusCode());
				throw new GlobleException("请求远程服务错误 状态码={}", _response.getStatusLine().getStatusCode());
			}
		} catch (ParseException | IOException e) {
			LogUtil.error("请求远程服务错误", e);
			throw new GlobleException("请求远程服务错误", e, "");
		}
	}

	/**
	 * 获取服务的随机访问ip
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月27日
	 * @param rpcService
	 * @return
	 */
	private String getRandomIp(RpcService rpcService) {
		Random random = new Random();
		List<IP> ipList = rpcService.getIpList();
		int randomPos = random.nextInt(ipList.size());
		IP ip = ipList.get(randomPos);
		String ipAddress = ip.getIpAddress();
		return ipAddress;
	}

	/**
	 * 获取远程服务的可用服务列表
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月27日
	 * @param serviceName
	 * @return
	 */
	private RpcService getRpcService(String serviceName) {
		LogUtil.debug("开始调用远程服务" + serviceName);

		try {
			HttpClient httpclient = HttpClients.createDefault();
			// 设置请求参数
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("serviceName", serviceName));
			HttpPost httpPost = new HttpPost(serviceRegistryBuilder.getServiceProvideUrl());
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			// 发送注册请求
			HttpResponse response = httpclient.execute(httpPost);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity(), "utf-8");
				LogUtil.info("获取服务结果=" + result);
				JSONObject object = (JSONObject) JSONObject.parseObject(result);
				Object status = object.get("status");
				if (status.equals(RpcConstant.SERVICE_EXISTENT)) {
					// 获取服务成功
					Gson g = new Gson();
					RpcService rpcService = g.fromJson(object.getString("service"), RpcService.class);
					return rpcService;
				} else {
					LogUtil.warn("获取远程服务失败：{}", result);
				}
			} else {
				LogUtil.info("服务注册错误 状态码={}", response.getStatusLine().getStatusCode());
			}
		} catch (ParseException | IOException e) {
			LogUtil.error("构建远程请求失败", e, "");
			throw new GlobleException("构建远程请求失败", e, "");
		}
		return null;
	}

}
