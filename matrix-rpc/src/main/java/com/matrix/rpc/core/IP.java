package com.matrix.rpc.core;


public class IP implements Comparable<IP> {

	public static final int WEIGHT_LOWER = 1;
	public static final int WEIGHT_NORMAL = 2;
	public static final int WEIGHT_HEIGHT = 5;

	private String ipAddress;
	private int weight;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Override
	public int compareTo(IP o) {
		return this.getIpAddress().compareTo(o.getIpAddress());
	}
	
	@Override
	public String toString() {
		return " ["+ ipAddress+","+weight+"] ";
		
	}
}
