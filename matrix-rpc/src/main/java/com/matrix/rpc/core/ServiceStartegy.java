package com.matrix.rpc.core;

import com.matrix.core.tools.StringUtils;

public enum ServiceStartegy {

	/**
	 * 随机策略
	 */
	RANDOM,
	/**
	 * 轮询策略
	 */
	ROTATION,
	/**
	 * 加权策略
	 */
	WEIGHT;

	public static ServiceStartegy getServiceStartegyByName(String startegyName) {
		ServiceStartegy[] ServiceStartegys = ServiceStartegy.values();
		for (ServiceStartegy serviceStartegy : ServiceStartegys) {
			if (StringUtils.equals(startegyName, serviceStartegy.toString())) {
				return serviceStartegy;
			}
		}
		return null;
	}

}
