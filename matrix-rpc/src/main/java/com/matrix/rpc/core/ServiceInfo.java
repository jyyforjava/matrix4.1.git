package com.matrix.rpc.core;

/**
 * 注册服务提供的信息
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月24日
 */
public class ServiceInfo {

	private String strategy;

	private String serviceName;

	private String ipAddress;

	private int weight;

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
