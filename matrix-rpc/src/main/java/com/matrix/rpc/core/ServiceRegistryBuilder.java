package com.matrix.rpc.core;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.springframework.beans.factory.InitializingBean;
import static com.matrix.rpc.core.RpcConstant.*;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.StringUtils;
import com.matrix.rpc.service.ExcuteService;
import com.matrix.rpc.service.ProvideUrlService;
import com.matrix.rpc.service.RegistryService;

/**
 * 服务注册表
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月23日
 */
public class ServiceRegistryBuilder implements InitializingBean {

	private List<ServiceInfo> serviceInfo;
	private Integer point;
	private String masterURl;

	private String registryUrl;

	private String serviceProvideUrl;

	@Override
	public void afterPropertiesSet() throws Exception {

		if (point == null) {
			throw new IllegalArgumentException("The parameter 'point' is necessary");
		}
		// 校验参数
		if (StringUtils.isBlank(masterURl)) {
			LogUtil.info("registryUrl is null");
			return;
		} else {
			registryUrl = masterURl + URI_REGISTRY;
			serviceProvideUrl = masterURl + URI_SERVICE_PROVIDE;
		}
		// 启动客户端
		startClient();
		// 注册服务
		registryService();

	}

	private void registryService() throws UnsupportedEncodingException, IOException, ClientProtocolException {
		// 创建http链接类
		HttpClient httpclient = HttpClients.createDefault();
		if (CollectionUtils.isNotEmpty(serviceInfo)) {
			for (ServiceInfo serviceInfo : serviceInfo) {
				// 设置请求参数
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("strategy", serviceInfo.getStrategy()));
				params.add(new BasicNameValuePair("serviceName", serviceInfo.getServiceName()));
				params.add(new BasicNameValuePair("ip", serviceInfo.getIpAddress()));
				params.add(new BasicNameValuePair("weight", serviceInfo.getWeight() + ""));
				HttpPost httpPost = new HttpPost(registryUrl);
				httpPost.setEntity(new UrlEncodedFormEntity(params));
				// 发送注册请求
				HttpResponse response = httpclient.execute(httpPost);
				LogUtil.info("开始注册服务{} 访问{}", serviceInfo.getServiceName(), registryUrl);
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					String result = EntityUtils.toString(response.getEntity(), "utf-8");
					LogUtil.info("服务注册结果=" + result);
				} else {
					LogUtil.info("服务注册错误 状态码={}", response.getStatusLine().getStatusCode());
				}
			}
		} else {
			LogUtil.info("service list is empty");
		}
	}

	private void startClient() throws Exception {
		// 新建服务
		Server server = new Server(point);
		ServletContextHandler context = new ServletContextHandler(server, "/");
		server.setHandler(context);
		// 注册服务
		context.addServlet(ExcuteService.class, URI_SERVICE_EXECUTE);
		// 启动监听
		server.start();
		LogUtil.info("Matrix RPC Client  start success listen in {}", point);
	}

	public List<ServiceInfo> getService() {
		return serviceInfo;
	}

	public void setService(List<ServiceInfo> service) {
		this.serviceInfo = service;
	}

	public List<ServiceInfo> getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(List<ServiceInfo> serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public String getMasterURl() {
		return masterURl;
	}

	public void setMasterURl(String masterURl) {
		this.masterURl = masterURl;
	}

	public String getRegistryUrl() {
		return registryUrl;
	}

	public void setRegistryUrl(String registryUrl) {
		this.registryUrl = registryUrl;
	}

	public String getServiceProvideUrl() {
		return serviceProvideUrl;
	}

	public void setServiceProvideUrl(String serviceProvideUrl) {
		this.serviceProvideUrl = serviceProvideUrl;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

}
