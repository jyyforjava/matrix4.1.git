package com.matrix.rpc.core;

import java.io.IOException;
import java.util.Map;

import org.apache.http.ParseException;

/**
 * 服务执行客户端
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月23日
 */
public interface RpcClient {

	/**
	 * 请求远程服务
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月24日
	 * @param serviceName
	 * @param param
	 * @return
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public Object requestService(String serviceName, Map<String, String> param) throws ParseException, IOException;

	/**
	 * 请求分片远程服务
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2018年9月24日
	 * @param serviceName
	 * @param param
	 * @return
	 */
	public Object requestSliceService(String serviceName, Map<String, String> param);

}
