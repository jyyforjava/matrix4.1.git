package com.matrix.rpc.service;

import static com.matrix.rpc.core.RpcConstant.SERVICE_ERROR;
import static com.matrix.rpc.core.RpcConstant.SERVICE_EXISTENT;
import static com.matrix.rpc.core.RpcConstant.SERVICE_NON_EXISTENT;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.matrix.core.tools.LogUtil;
import com.matrix.rpc.core.RpcService;
import com.matrix.rpc.core.ServicesMapping;

public class ProvideUrlService extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		JSONObject ret = new JSONObject();
		try {
			String serviceName = req.getParameter("serviceName");
			RpcService rpcService = ServicesMapping.getRpcServiceByName(serviceName);
			if (rpcService == null) {
				LogUtil.debug("{}服务不存在", serviceName);
				ret.put("status", SERVICE_NON_EXISTENT);
			} else {
				Gson g = new Gson();
				String service = g.toJson(rpcService);
				ret.put("service", service);
				ret.put("status", SERVICE_EXISTENT);
				LogUtil.debug("获取到服务={}", service);
			}

		} catch (Exception ex) {
			ret.put("status", SERVICE_ERROR);
			ret.put("error", ex.getMessage());
			LogUtil.error("获取服务出错", ex, "");
		}
		resp.getWriter().println(ret.toString());
	}
}
