package com.matrix.rpc.service;

import static com.matrix.rpc.core.RpcConstant.SERVICE_ERROR;
import static com.matrix.rpc.core.RpcConstant.SERVICE_EXISTENT;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.matrix.core.tools.LogUtil;
import com.matrix.core.tools.WebUtil;
import com.matrix.rpc.core.RpcExecuter;

/**
 * 执行服务
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月23日
 */
public class ExcuteService extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		JSONObject ret = new JSONObject();
		resp.setHeader("Content-type", "text/html;charset=UTF-8");
		try {
			String serviceName = req.getParameter("serviceName");
			LogUtil.info("客户端请求执行器服务{}", serviceName);
			Object executer = WebUtil.getApplicationContext().getBean(serviceName);
			if (executer != null) {
				Enumeration<String> enumeration = req.getParameterNames();
				Map<String, String> paramMap = new HashMap<>();
				for (; enumeration.hasMoreElements();) {
					String paramName = enumeration.nextElement();
					paramMap.put(paramName, req.getParameter(paramName));
				}
				LogUtil.info("获取到请求 参数", paramMap);
				RpcExecuter rpcExecuter = (RpcExecuter) executer;
				//调用执行器方法
				Map<String, String> resultMap = rpcExecuter.execute(paramMap);
				Gson g = new Gson();
				String result=g.toJson(resultMap);
				LogUtil.info("执行成功返回请求参数={}", result);
				ret.put("status", SERVICE_EXISTENT);
				ret.put("result",result);
			} else {
				LogUtil.info("在客户端没有找到可处理" + serviceName + "请求的bean");
				ret.put("status", SERVICE_ERROR);
				ret.put("msg", "在客户端没有找到可处理" + serviceName + "请求的bean");
			}

		} catch (Exception ex) {
			LogUtil.error("执行执行失败", ex, "");
			ret.put("status", SERVICE_ERROR);
			ret.put("error", ex.getMessage());
		}
		resp.getWriter().println(ret.toString());
	}
}
