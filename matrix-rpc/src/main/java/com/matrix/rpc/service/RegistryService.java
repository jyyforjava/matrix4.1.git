package com.matrix.rpc.service;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.matrix.rpc.core.ServicesMapping;

/**
 * 服务管理中心
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2018年9月23日
 */
public class RegistryService extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		JSONObject ret = new JSONObject();
		resp.setHeader("Content-type", "text/html;charset=UTF-8");
		try {
			String strategy = req.getParameter("strategy");
			String serviceName = req.getParameter("serviceName");
			String ip = req.getParameter("ip");
			Integer weight = Integer.parseInt(req.getParameter("weight"));

			ServicesMapping.registerService(serviceName, ip, strategy, weight);

			ret.put("status", "200");
			ret.put("msg", serviceName + "注册成功");

		} catch (Exception ex) {
			ret.put("status", "7777");
			ret.put("error", ex.getMessage());
		}
		resp.getWriter().println(ret.toString());
	}
}
