package com.matrix.sso.bean;

import org.springframework.util.StringUtils;

/**
 * sso登录授权bean
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年9月16日
 */
public class SSOAuthorize {

	/** 应用的id */
	private String aapId;
	/** 回调地址 */
	private String redirectUrl;
	/** sso取数Token */
	private String ssoToken;

	public String getAapId() {
		return aapId;
	}

	public void setAapId(String aapId) {
		this.aapId = aapId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getSsoToken() {
		return ssoToken;
	}

	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}

	/**
	 * 构建一个回调地址
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月16日
	 * @return
	 */
	public String buildRedirectUrl() {
		// 校验参数
		if (StringUtils.isEmpty(redirectUrl)) {
			throw new IllegalArgumentException("'redirectUrl' is necessary but  is null");
		}

		if (StringUtils.isEmpty(ssoToken)) {
			throw new IllegalArgumentException("'ssoToken' is necessary but  is null");
		}

		if (redirectUrl.contains("?")) {
			redirectUrl = redirectUrl + "&ssotoken=" + ssoToken;
		} else {
			// 不带参数的url
			redirectUrl = redirectUrl + "?ssotoken=" + ssoToken;
		}
		return redirectUrl;
	}

}
