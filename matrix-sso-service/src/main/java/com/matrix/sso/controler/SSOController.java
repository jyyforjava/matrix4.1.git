package com.matrix.sso.controler;

import static com.matrix.sso.constant.Constant.INVALID_TIME;
import static com.matrix.sso.constant.Constant.PAGE_404;
import static com.matrix.sso.constant.Constant.PAGE_LOGIN;
import static com.matrix.sso.constant.Constant.USER_INFO;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.matrix.core.authority.strategy.LoginStrategy;
import com.matrix.core.authority.strategy.SsoAccountPwdLoginStrategy;
import com.matrix.core.bean.SysUsers;
import com.matrix.core.exception.GlobleException;
import com.matrix.core.pojo.AjaxResult;
import com.matrix.core.service.SysUsersService;
import com.matrix.core.tools.WebUtil;
import com.matrix.core.web.BaseController;
import com.matrix.sso.bean.SSOAuthorize;
import com.matrix.sso.cache.Cache;
import com.matrix.sso.service.SSOService;

@Controller
@RequestMapping(value = "sso/connect")
public class SSOController extends BaseController {

	Logger log = Logger.getLogger(SSOController.class);

	@Autowired
	SysUsersService userService;

	@Autowired
	SSOService ssoService;

	@Autowired
	Cache menCache;

	/**
	 * 业务系统申请sso登录
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月15日
	 * @param appId
	 *            应用的appid，系统在注册的时候获得
	 * @param redirectUrl
	 *            验证成功后要跳转的业务回调地址
	 * @param response
	 * @return
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/authorize")
	public String authorize(SSOAuthorize ssoAuthorize, HttpServletResponse response) {
		if (ssoService.isValidityApp(ssoAuthorize)) {
			// 判断用户是否已经登录
			if (isLogined()) {
				Future<Boolean> storeFuture = createAutgoruzeCache(ssoAuthorize);
				try {
					if (storeFuture.get()) {
						// 服务器缓存保存成功
						response.sendRedirect(ssoService.getTargetUrl(ssoAuthorize));
					} else {
						throw new GlobleException("服务器缓存保存失败");
					}
				} catch (InterruptedException | ExecutionException | IOException e) {
					throw new GlobleException("服务器缓存保存失败");
				}
			} else {
				// 没有登录的用户跳转到登录界面
				WebUtil.setRequestAttribute("redirectUrl", ssoAuthorize.getRedirectUrl());
				WebUtil.setRequestAttribute("appId", ssoAuthorize.getAapId());
				return PAGE_LOGIN;
			}
			return null;
		} else {
			return PAGE_404;
		}
	}

	/**
	 * FIXME 实现思路 1、系统管理 对系统进行注册 2、用户管理 用户有可登录的系统 3、对应用进行登录授权
	 */
	@RequestMapping(value = "/login")
	@ResponseBody
	public AjaxResult login(String account, String password, SSOAuthorize ssoAuthorize) {
		log.info("userName:" + account + " password:" + password);
		SysUsers user = new SysUsers();
		user.setSuAccount(account);
		user.setSuPassword(password);
		// 调用用户登录验证
		LoginStrategy loginStrategy = new SsoAccountPwdLoginStrategy(user, userService);
		SysUsers loginUser = (SysUsers) loginStrategy.login();

		if (loginUser != null) {
			WebUtil.setSessionAttribute(USER_INFO, loginUser);
			// 登录成功在缓存中新增数据
			Future<Boolean> storeFuture = createAutgoruzeCache(ssoAuthorize);
			try {
				if (storeFuture.get()) {
					// 服务器缓存保存成功
					return new AjaxResult(AjaxResult.STATUS_SUCCESS, ssoService.getTargetUrl(ssoAuthorize), "登录成功");
				} else {
					throw new GlobleException("服务器缓存保存失败");
				}
			} catch (InterruptedException | ExecutionException e) {
				throw new GlobleException("服务器缓存保存失败");
			}
		} else {
			return new AjaxResult(AjaxResult.STATUS_FAIL, "登录失败");
		}

	}

	/**
	 * 创建认证成功的缓存
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月16日
	 * @param ssoAuthorize
	 * @return
	 */
	private Future<Boolean> createAutgoruzeCache(SSOAuthorize ssoAuthorize) {
		// 已经登录的用户直接跳转到要求的回调地址
		ssoAuthorize.setSsoToken(UUID.randomUUID().toString());
		// 在缓存中新增用户数据
		Future<Boolean> storeFuture = menCache.addCache(ssoAuthorize.getSsoToken(),
				WebUtil.getSessionAttribute(USER_INFO), INVALID_TIME);
		return storeFuture;
	}

	private boolean isLogined() {
		return WebUtil.getSessionAttribute(USER_INFO) != null;
	}

}