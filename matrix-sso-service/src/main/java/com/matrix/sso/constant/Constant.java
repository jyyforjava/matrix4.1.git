package com.matrix.sso.constant;

public class Constant {
	public static final String USER_INFO = "user_info";
	public final static String PAGE_404 = "/404";
	public final static String PAGE_LOGIN = "/login";
	// 登录失效时间
	public final static int INVALID_TIME = 60 * 1;
	public static final String USER_TOKEN = "user_token";
}
