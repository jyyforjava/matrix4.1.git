package com.matrix.sso.service.impl;

import org.springframework.stereotype.Service;

import com.matrix.core.exception.GlobleException;
import com.matrix.sso.bean.SSOAuthorize;
import com.matrix.sso.service.SSOService;

@Service("SSOServiceImpl")
public class SSOServiceImpl implements SSOService {

	@Override
	public boolean isValidityApp(SSOAuthorize ssoAuthorize) {
		/**
		 * TODO step1.redirectUrl 必须是appid配置好的域名或者ip下的url 否则不通过
		 * step2.从数据库中获取id信息，判断是否是已经注册过的系统
		 */
		return true;
	}

	/**
	 * 获取跳转到业务回调地址
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月15日
	 * @param redirectUrl
	 * @param response
	 */
	@Override
	public String getTargetUrl(SSOAuthorize ssoAuthorize) {
		if (isValidityApp(ssoAuthorize)) {
			// 带参数url
			return ssoAuthorize.buildRedirectUrl();
		} else {
			throw new GlobleException("无效的回调地址");
		}

	}
}
