package com.matrix.sso.service;

import com.matrix.sso.bean.SSOAuthorize;

public interface SSOService {


	boolean isValidityApp(SSOAuthorize ssoAuthorize);

	String getTargetUrl(SSOAuthorize ssoAuthorize);

}
