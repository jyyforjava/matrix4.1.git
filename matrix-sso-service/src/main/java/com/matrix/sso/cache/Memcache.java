package com.matrix.sso.cache;

import static org.springframework.util.Assert.notNull;

import java.net.InetSocketAddress;
import java.util.concurrent.Future;

import org.springframework.beans.factory.InitializingBean;

import net.spy.memcached.MemcachedClient;

public class Memcache implements Cache, InitializingBean {

	private static final Memcache memcache = new Memcache();

	private String hostname;

	private int port;

	private MemcachedClient mcc = null;

	private Memcache() {
	}

	public static Memcache getInstance() {
		return memcache;
	}

	@Override
	public Future<Boolean> addCache(String key, Object value, int invalidTime) {
		// 存储数据
		Future<Boolean> storeFuture = mcc.set(key, invalidTime, value);
		return storeFuture;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// 校验参数
		notNull(hostname, "Property 'hostname' is required");
		notNull(port, "port 'hostname' is required");

		// TODO 校验缓存是否连接正常
		mcc = new MemcachedClient(new InetSocketAddress(hostname, port));
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
