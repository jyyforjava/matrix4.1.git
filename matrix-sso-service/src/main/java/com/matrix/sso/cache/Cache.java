package com.matrix.sso.cache;

import java.util.concurrent.Future;

public interface Cache {
	/**
	 * 在远程服务器上新增用户缓存 失效时间为
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月16日
	 * @param appId
	 * @param redirectUrl
	 * @param response
	 * @param loginUser
	 * @return
	 */
	public Future<Boolean> addCache(String key, Object value, int invalidTime);

}
