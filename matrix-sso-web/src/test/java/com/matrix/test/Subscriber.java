package com.matrix.test;

public interface Subscriber {

	String receive(String message);

}
