package com.matrix.test;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.matrix.sso.controler.SSOController;

import junit.framework.TestCase;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({ "classpath:spring-mvc-context.xml" })
public class SSOControllerTest {

	@Autowired
	SSOController sSOController;
	private Mockery moke = null;

	/**
	 * 获取配置文件信息测试
	 * 
	 * @author JIANGYOUYAO
	 * @throws IOException
	 * @email 935090232@qq.com
	 * @date 2017年9月5日
	 */
	@Test
	public void testAuthorize() throws IOException {
		moke = new Mockery();
		final HttpServletResponse response = moke.mock(HttpServletResponse.class);

		// 定义预期行为
		moke.checking(new Expectations() {
			{
				// 对这Mock对象方法调用时的期望，如此处设置希望输入参数为”root”和”root”时，返回true
				oneOf(response).sendRedirect("redirectUrl");
			}

		});

		response.sendRedirect("redirectUrl");
	}

	@Test
	public void testOneSubscriberReceivesAMessage() {
		Mockery context = new Mockery();
		// set up
		final Subscriber subscriber = context.mock(Subscriber.class);

		Publisher publisher = new Publisher();
		publisher.add(subscriber);

		final String message = "message";

		// expectations
		context.checking(new Expectations() {
			{
					oneOf(subscriber).receive(message);
					will(returnValue("123"));
			}
		});

		// execute
		publisher.publish(message);

		// verify
		context.assertIsSatisfied();
	}

}
