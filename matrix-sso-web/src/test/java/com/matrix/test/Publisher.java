package com.matrix.test;

import java.util.ArrayList;
import java.util.List;

public class Publisher {

	List<Subscriber> list=new ArrayList<>();
		
	public void add(Subscriber subscriber) {
		list.add(subscriber);
	}

	public void publish(String message) {
		for (Subscriber subscriber : list) {
			System.out.println(subscriber.receive(message));
		}
		
	}

}
