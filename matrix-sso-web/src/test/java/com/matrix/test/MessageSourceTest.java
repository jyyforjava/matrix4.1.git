package com.matrix.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;

public class MessageSourceTest extends BaseTest {

	@Autowired
	MessageSource messageSource;

	/**
	 * 获取配置文件信息测试
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月5日
	 */
	@Test
	public void testMessage() {
		Assert.assertEquals(messageSource.getMessage("mobileIsNotExsit", new String[] { "18390563333" }, Locale.US),
				"mobile is invalida 18390563333");
	}

	@Test
	public void testSystemProperty() {
		Properties b = new Properties();
		b.setProperty("test", "tt");
		System.setProperties(b);
		System.out.println(System.getProperty("test"));
	}
	
	public static void main(String[] args) {
		MessageSourceTest t=new MessageSourceTest();
		t.sayHellow();
		
	}
	
	public void sayHellow(){
		System.out.println("hellow");
		sayHellow();
	}

}
