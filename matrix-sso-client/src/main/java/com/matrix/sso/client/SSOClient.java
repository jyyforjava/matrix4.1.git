package com.matrix.sso.client;

public interface SSOClient {

	/**
	 * 在缓存获取一个object对象
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月16日
	 * @param key
	 * @return
	 */
	public Object getCachedObject(String key);

	/**
	 * 获取授权跳转地址
	 * 
	 * @author JIANGYOUYAO
	 * @email 935090232@qq.com
	 * @date 2017年9月16日
	 * @return
	 */
	String getSSOAuthorizeUrl(String redirectUrl);

}
