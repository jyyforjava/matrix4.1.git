package com.matrix.sso.client;

import static org.springframework.util.Assert.notNull;

import java.net.InetSocketAddress;

import org.springframework.beans.factory.InitializingBean;

import net.spy.memcached.MemcachedClient;

public class MemcacheSSOClient implements SSOClient, InitializingBean {

	private static final MemcacheSSOClient memcache = new MemcacheSSOClient();

	private String hostname;

	private int port;

	private String authorizeUrl;

	private String authorizeAppid;

	private MemcachedClient mcc = null;

	private MemcacheSSOClient() {
	}

	public static MemcacheSSOClient getInstance() {
		return memcache;
	}

	@Override
	public Object getCachedObject(String key) {
		Object value = mcc.get(key);
		return value;
	}

	/**
	 * bean创建后调用
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// 校验参数
		notNull(hostname, "Property 'hostname' is required");
		notNull(port, "Property 'port' is required");
		notNull(authorizeUrl, "Property 'authorizeUrl' is required");
		notNull(authorizeAppid, "Property 'authorizeAppid' is required");

		// TODO 校验缓存是否连接正常
		mcc = new MemcachedClient(new InetSocketAddress(hostname, port));
	}

	@Override
	public String getSSOAuthorizeUrl(String redirectUrl) {
		String authorizetUrl = authorizeUrl + "?appId=" + authorizeAppid + "&redirectUrl=" + redirectUrl;
		return authorizetUrl;
	}

	public String getAuthorizeAppid() {
		return authorizeAppid;
	}
	
	public String getAuthorizeUrl() {
		return authorizeUrl;
	}

	public void setAuthorizeAppid(String authorizeAppid) {
		this.authorizeAppid = authorizeAppid;
	}

	public void setAuthorizeUrl(String authorizeUrl) {
		this.authorizeUrl = authorizeUrl;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
