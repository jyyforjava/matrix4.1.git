package com.matrix.sso.client;

import static org.junit.Assert.*;

import org.junit.Test;

public class MemcacheSSOClientTest {

	MemcacheSSOClient ssoClient = MemcacheSSOClient.getInstance();

	public void initClient() {
		ssoClient.setHostname("47.75.12.127");
		ssoClient.setAuthorizeUrl("http://127.0.0.1/sso");
		ssoClient.setAuthorizeAppid("rt123");
		ssoClient.setPort(11011);
	}

	@Test
	public void testGetCachedObject() {
		initClient();
		try {
			ssoClient.afterPropertiesSet();
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertNull(ssoClient.getCachedObject("test"));
	}

	@Test
	public void testGetAuthorizeUrl() {
		assertEquals(ssoClient.getAuthorizeUrl() + "?appId=" + ssoClient.getAuthorizeAppid() + "&redirectUrl="
				+ "www.baidu.com", ssoClient.getSSOAuthorizeUrl("www.baidu.com"));
	}

}
