package com.matrix.bean.anotations;

/**
 * 系统错误码
 * 
 * @author JIANGYOUYAO
 * @email 935090232@qq.com
 * @date 2017年11月28日
 */
public interface SystemErrorCode {
	
	/** =======系统错误码=========== **/
	
	
	/** 系统未知异常 */
	String SYSTEM_UNKNOW_ERROR = "700001";
	
	/** 系统运行异常，参数{0}不能为空 */
	String SYSTEM_NULLF_ERROR = "700002";
	
	
	
	
	/** =======业务错误码=========== **/
	
	/** 文章管理模块 **/
	public interface Bolg{
		String BIZ_CODE="900";
		
		/** 文章未发布 **/
		String ARTICLE_NOT_PUBLISH =BIZ_CODE+"000";
		
	}
	
	

}
